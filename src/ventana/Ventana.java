package ventana;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import objetos.Panel;
import objetos.ColorChooserButton;
import objetos.Slider;
import objetos.BotonRotar;

public class Ventana extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String objeto;
	Color color_seleccionado = Color.red;
	Panel panel_fondo;
		
	public Ventana()
	{
		super("MiniCAD");
		JPanel panel_principal = new JPanel();
		panel_principal.setLayout(null);
		panel_principal.setBounds(0, 0, 800, 32);
		
		Slider slider = new Slider();
		slider.setBounds(650, 0, 100, 32);
		
		BotonRotar boton_izquierda = new BotonRotar("+");
		boton_izquierda.setBounds(400, 0, 32, 32);
		BotonRotar boton_derecha = new BotonRotar("-");
		boton_derecha.setBounds(450, 0, 32, 32);
		
		this.panel_fondo = new Panel(this);
		this.panel_fondo.setBounds(0, 32, 800, 568);
		panel_fondo.setBackground(Color.DARK_GRAY);
		panel_fondo.setSlider(slider);
		panel_fondo.setRotador(boton_izquierda, "+");
		panel_fondo.setRotador(boton_derecha, "-");
		panel_principal.add(this.panel_fondo);
		
                JComboBox Combo =new JComboBox();
                
                
                
		JButton boton_rectangulo = new JButton();
		boton_rectangulo.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/rectangulo.png")));
		boton_rectangulo.setBounds(0, 0, 32, 32);
		boton_rectangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				objeto = "Rectangulo";
				panel_fondo.setObjeto(objeto);
				
			}
		});
		
			
		JButton boton_linea = new JButton();
		boton_linea.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/linea_32x32.png")));
		boton_linea.setBounds(32, 0, 32, 32);
		boton_linea.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				objeto = "Linea";
				panel_fondo.setObjeto(objeto);
				
			}
		});
		
		JButton boton_circulo = new JButton();
		boton_circulo.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/circulo1_32x32.png")));
		boton_circulo.setBounds(64, 0, 32, 32);
		boton_circulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				objeto = "Circulo";
				panel_fondo.setObjeto(objeto);
				
			}
		});
		
		JButton boton_triangulo = new JButton();
		boton_triangulo.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/triangulo1_32x32.png")));
		boton_triangulo.setBounds(96, 0, 32, 32);
		boton_triangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				objeto = "Triangulo";
				panel_fondo.setObjeto(objeto);
				
			}
		});
		
//		JButton boton_libre = new JButton();
//		boton_libre.setIcon(new ImageIcon(ClassLoader.getSystemResource("varios/mano32_32.png")));
//		boton_libre.setBounds(128, 0, 32, 32);
//		boton_libre.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent evt) {
//				objeto = "Libre";
//				panel_fondo.setObjeto(objeto);
//				
//			}
//		});
//		
		ColorChooserButton ccb = new ColorChooserButton(Color.black);
		ccb.setBounds(160, 0, 32, 32);
		ccb.addColorChangedListener(new ColorChooserButton.ColorChangedListener() {
		    @Override
		    public void colorChanged(Color newColor) {
		    	color_seleccionado = newColor;
		    	panel_fondo.setColor(newColor);
		    	
		    	System.out.println("Desde Ventana: " + color_seleccionado.toString());
		            
		    }
		});
		
		JLabel lblcolor = new JLabel("Cambiar Color");
		lblcolor.setBounds(192, 0, 150, 32);
		
		panel_principal.add(boton_rectangulo);
		panel_principal.add(boton_linea);
		panel_principal.add(boton_circulo);
		panel_principal.add(boton_triangulo);
//		panel_principal.add(boton_libre);
		panel_principal.add(ccb);
		panel_principal.add(lblcolor);
		panel_principal.add(slider);
		panel_principal.add(boton_izquierda);
		panel_principal.add(boton_derecha);
		
		this.add(panel_principal);
		this.setSize(800, 600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}
}
