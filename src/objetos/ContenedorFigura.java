package objetos;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import javax.swing.JPanel;

import eventos.Arrastrar;

public class ContenedorFigura extends JPanel {
	
	public Arrastrar ar;
	private int x_inicial = -1;
    private int x_final = -1;
    private int y_inicial = -1;
    private int y_final = -1;
    private int ancho;
    private int alto;
    private int grados = 0;
	
	public ContenedorFigura(int xInicial, int xFinal, int yInicial, int yFinal) {
		this.x_inicial = xInicial;
		this.x_final = xFinal;
		this.y_inicial = yInicial;
    	this.y_final = yFinal;
    	this.alto = this.y_final - this.y_inicial;
    	this.ancho = this.x_final - this.x_inicial;
		//this.ar = new Arrastrar(this);
    	//this.addMouseMotionListener(ar);
	}

	public void Mover(int x, int y) {
		// TODO Auto-generated method stub
		if ((y > 32) && ((y+alto) < 568) && (x > 0) && ((x+ancho) < 800)) {
			this.x_inicial = x;
			this.y_inicial = y;
			this.setLocation(x, y);
			//this.panel.setLocation(x + x_inicial, y_inicial);
		}
	}
	
	public void setGrados(int grados) {
		this.grados = grados;
		System.out.println("Contenedor grados " + this.grados);
		paint(super.getGraphics());
		//repaint();
	}
	
	@Override
    public void paint(Graphics g) {
        super.paint(g); //se borra el contenido anterior
 
        double r = Math.toRadians(grados); //se convierte a radianes lo grados
 
        AffineTransform at = new AffineTransform();
        at.rotate(r, 100, 100); //se asigna el angulo y centro de rotacion
        ((Graphics2D) g).setTransform(at);
 
        //se dibuja
        g.setColor(Color.BLUE);
        g.drawRect(50, 50, 100, 100);
 
    }
}
