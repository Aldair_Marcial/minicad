package objetos;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;

import eventos.ListenerArrastre;
import figuras.Circulo;
import figuras.Linea;
import figuras.Puntos;
import figuras.Rectangulo;
import figuras.Triangulo;
import objetos.Slider;
import objetos.BotonRotar;

public class Panel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int xInicial;
	private int xFinal;
	
	private int yInicial;
	private int yFinal;
	
	private String objeto = "";
	private Color color = Color.BLACK;
	private JFrame ventana;
	private Slider slider;
	private BotonRotar rotar_izquierda;
	private BotonRotar rotar_derecha;
	
	public Panel(JFrame ventana)
	{
		this.setLayout(null);
		this.ventana = ventana;
		ListenerArrastre la = new ListenerArrastre(this);
		
		//this.setBackground(Color.white);
		this.addMouseMotionListener(la);
		
		/*this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 1) {
					enaClick = true;
				}
			}
		});*/
	}
	
	public void setCoords(int xInicial, int yInicial, int xFinal, int yFinal)
	{
		this.xInicial = xInicial;
		this.xFinal = xFinal;
		this.yInicial = yInicial;
		this.yFinal = yFinal;
	}
	
	public void setObjeto(String objeto)
	{
		this.objeto = objeto;

	}
	
	public void setColor(Color color)
	{
		this.color = color;
	}
	
	public void setSlider(Slider slider)
	{
		this.slider = slider;
	}
	
	public void setRotador(BotonRotar boton, String accion) {
		if (accion == "+")
			this.rotar_izquierda = boton;
		else
			this.rotar_derecha = boton;
	}
	
	public void Dibujar()
	{
		//System.out.println("Xi: " + xInicial + " Yi: " + yInicial + " Xf: " + xFinal + " Yf: " + yFinal + " Objeto " + this.objeto);
		
		switch(this.objeto)
		{
		case "Rectangulo":
			//JPanel panel = new JPanel();
			ContenedorFigura contenedor = new ContenedorFigura(xInicial, xFinal, yInicial, yFinal);
			contenedor.setLocation(xInicial, yInicial);
			//panel.setLayout(null);
			Rectangulo r = new Rectangulo(xInicial, xFinal, yInicial, yFinal);
			//r.setPanel(this);
			r.setColor(this.color);
			r.setSlider(this.slider);
			r.setRotador(rotar_izquierda, "+");
			r.setRotador(rotar_derecha, "-");
			this.slider.setObjeto(r);
			this.rotar_izquierda.setObjeto(r);
			this.rotar_derecha.setObjeto(r);
			//contenedor.add(r);
			//panel.setBounds(xInicial, yInicial, xFinal-xInicial, yFinal-yInicial);
			//contenedor.setLocation(0, 32);
			this.ventana.add(r);
			//this.add(contenedor);
			r = null;
			break;
		case "Triangulo":
			Triangulo t = new Triangulo(xInicial, xFinal, yInicial, yFinal);
			t.setColor(color);
			t.setSlider(this.slider);
			t.setRotador(rotar_izquierda, "+");
			t.setRotador(rotar_derecha, "-");
			this.slider.setObjeto(t);
			this.rotar_izquierda.setObjeto(t);
			this.rotar_derecha.setObjeto(t);
			this.ventana.add(t);
			t = null;
			break;
		case "Circulo":
			Circulo c = new Circulo(xInicial, xFinal, yInicial, yFinal);
			c.setColor(color);
			c.setSlider(this.slider);
			c.setRotador(rotar_izquierda, "+");
			c.setRotador(rotar_derecha, "-");
			this.slider.setObjeto(c);
			this.rotar_izquierda.setObjeto(c);
			this.rotar_derecha.setObjeto(c);
			this.ventana.add(c);
			c = null;
			break;
		case "Linea":
			Linea l = new Linea(xInicial, xFinal, yInicial, yFinal);
			l.setColor(color);
			l.setSlider(this.slider);
			l.setRotador(rotar_izquierda, "+");
			l.setRotador(rotar_derecha, "-");
			this.slider.setObjeto(l);
			this.rotar_izquierda.setObjeto(l);
			this.rotar_derecha.setObjeto(l);
			this.ventana.add(l);
			l = null;
			break;
		}
	}
}
