package figuras;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import objetos.BotonRotar;
import objetos.ContenedorFigura;
import objetos.Slider;
import eventos.Arrastrar;
import eventos.EventosObjetos;

public class Circulo extends Canvas implements EventosObjetos {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Arrastrar ar;
	private int x_inicial = -1;
    private int x_final = -1;
    private int y_inicial = -1;
    private int y_final = -1;
    private int ancho;
    private int alto;
    private int ancho_inicial;
    private int alto_inicial;
    
    public EventosObjetos objeto;
    private Slider slider;
    private int escala = 100;
    private int angulo = 0;
    private BotonRotar rotar_izquierda;
    private BotonRotar rotar_derecha;
    
    //private LinkedList<Point2D> puntos = new LinkedList<Point2D>();
    private Puntos puntos;
    private Color color = Color.BLACK;
    public Circulo(int x_inicial, int x_final, int y_inicial, int y_final) {
    	this.setBackground(color.DARK_GRAY);
    	
    	this.objeto = this;
    	this.ar = new Arrastrar(this);
    	this.addMouseMotionListener(ar);
    	this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 1) {
					slider.setObjeto(objeto);
					slider.setValue(escala);
					rotar_izquierda.setObjeto(objeto);
					rotar_derecha.setObjeto(objeto);
				}
			}
		});
    	
    	this.ancho_inicial = Math.abs(x_final - x_inicial);
		this.alto_inicial = Math.abs(y_final - y_inicial);
    	
    	this.puntos = new Puntos();
    	this.x_inicial = x_inicial;
    	this.x_final = x_final;
    	this.y_inicial = y_inicial;
    	this.y_final = y_final;
    	this.Calcular();
    }
	
	@Override
	public void Mover(int x, int y) {
		// TODO Auto-generated method stub
		if ((y > 32) && ((y+alto) < 568) && (x > 0) && ((x+ancho) < 800)) {
			this.x_inicial = x;
			this.y_inicial = y;
			this.setLocation(x, y);
		}
	}

	@Override
	public void Tamano(int width, int height) {
		// TODO Auto-generated method stub
		this.setSize(width, height);
		this.ancho = width;
		this.alto = height;
		this.x_final = this.x_inicial + this.ancho;
		this.y_final = this.y_inicial + this.alto;
	}

	@Override
	public void Calcular() {
		// TODO Auto-generated method stub
		int tmp;
		double x, y;
		double x_vector, y_vector;
    	double centro_x, centro_y;
    	double r;
    	int teta=0;

		if (this.x_inicial > this.x_final)
		{
			tmp = this.x_final;
			this.x_final = this.x_inicial;
			this.x_inicial = tmp;
		}
		
		if (this.y_inicial > this.y_final)
		{
			tmp = this.y_final;
			this.y_final = this.y_inicial;
			this.y_inicial = tmp;
		}
		
		this.ancho = this.x_final - this.x_inicial;
		this.alto = this.y_final - this.y_inicial;
		
		this.Tamano(this.ancho, this.alto);
		this.setLocation(this.x_inicial, this.y_inicial + 32);
		
		double porcentaje_ancho = (ancho * 40) / 100;
		double porcentaje_alto = (alto * 40) / 100;
		int valor_ancho = (int) (ancho - porcentaje_ancho);
		int valor_alto = (int) (alto - porcentaje_alto);
		
		//x_inicial = 0;
		//x_final = valor_ancho;
		//y_inicial = 0;
		//y_final = valor_alto;
		
		int xi, xf, yi, yf;
		
		xi = 0;
		xf = valor_ancho;
		yi = 0;
		yf = valor_alto;
		
		//x_vector = (x_final - x_inicial);
    	//y_vector = (y_final - y_inicial);
    	
    	x_vector = (xf - xi);
    	y_vector = (yf - yi);
    	
    	r = Math.sqrt(Math.pow((x_vector/2),2) + Math.pow((y_vector/2),2));
    	
    	centro_x = (x_vector / 2) + (porcentaje_ancho/2);
    	centro_y = (y_vector / 2) + (porcentaje_alto/2);
        
    	for (teta = 0; teta <=360; teta++) {
    		x = (xi + centro_x) + (r * Math.cos(teta));
    		y = (yi + centro_y) + (r * Math.sin(teta));
    		puntos.addPunto((int)x, (int)y);
    	}
	}

	public void paint(Graphics g)
    {
		//for (int i = 0; i < this.puntos.getSize(); i++)
        //{
            dibujaTrazo(g);
        //}
    }
	
	public void repaint()
	{
		for (int i = 0; i < this.puntos.getSize(); i++)
        {
            dibujaTrazo(this.getGraphics());
        }
	}
	
	public void update(Graphics g)
    {
        paint(g);
    }
	
	private void dibujaTrazo(Graphics g)
    {	
        g.setColor(this.color);
        
    	Point2D p0 = this.puntos.getPunto(0);
        for (int i = 0; i < this.puntos.getSize() - 1; i++)
        {
            Point2D p1 = this.puntos.getPunto(i);
            g.drawLine((int) p0.getX(), (int) p0.getY(), (int) p1.getX(),
                    (int) p1.getY());
            p0 = p1;
        }
    }
	
	public int getX()
	{
		return this.x_inicial;
	}
	
	public int getY()
	{
		return this.y_inicial;
	}
	
	public int getWidth()
	{
		//System.out.println(this.width);
		return this.ancho;
	}
	
	public int getHeight()
	{
		return this.alto;
	}
	
	@Override
	public void Recalcular() {
		// TODO Auto-generated method stub
		Graphics g = this.getGraphics();
		g.clearRect(0, 0, ancho, alto);
		puntos = null;
		this.puntos = new Puntos();
		Calcular();
		this.paint(g);
	}
	
	public Graphics getGraficos() {
		return this.getGraphics();
	}

	@Override
	public void dibujar() {
		// TODO Auto-generated method stub
		this.repaint();
	}

	@Override
	public void setColor(Color color) {
		// TODO Auto-generated method stub
		this.color = color;
	}

	@Override
	public void setSlider(Slider slider) {
		// TODO Auto-generated method stub
		this.slider = slider;
		this.slider.setValue(this.escala);
	}

	@Override
	public void setEscala(int escala) {
		// TODO Auto-generated method stub
		this.escala = escala;
	}

	@Override
	public int getAngulo() {
		// TODO Auto-generated method stub
		return this.angulo;
	}

	@Override
	public void setAngulo(int angulo) {
		// TODO Auto-generated method stub
		this.angulo = angulo;
	}

	@Override
	public void setRotador(BotonRotar rotador, String accion) {
		// TODO Auto-generated method stub
		//if (accion == "+")
			this.rotar_izquierda = rotador;
		//else
			this.rotar_derecha = rotador;
	}

	@Override
	public void setPanel(ContenedorFigura panel) {
		// TODO Auto-generated method stub
	}

	@Override
	public ContenedorFigura getPanel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Actualizar(Graphics g) {
		// TODO Auto-generated method stub
		this.paint(g);
	}

	@Override
	public int getAnchoInicial() {
		// TODO Auto-generated method stub
		return ancho_inicial;
	}

	@Override
	public int getAltoInicial() {
		// TODO Auto-generated method stub
		return alto_inicial;
	}

}
