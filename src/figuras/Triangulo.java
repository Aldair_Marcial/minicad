package figuras;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import objetos.BotonRotar;
import objetos.ContenedorFigura;
import objetos.Slider;
import eventos.Arrastrar;
import eventos.EventosObjetos;

public class Triangulo extends Canvas implements EventosObjetos {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Arrastrar ar;
	private int x_inicial = -1;
    private int x_final = -1;
    private int y_inicial = -1;
    private int y_final = -1;
    private int ancho;
    private int alto;
    private int ancho_inicial;
    private int alto_inicial;
    
    public EventosObjetos objeto;
    private Slider slider;
    private int escala = 100;
    private int angulo = 0;
    private BotonRotar rotar_izquierda;
    private BotonRotar rotar_derecha;
    
    private Puntos puntos;
    private Color color = Color.BLACK;
    
    public Triangulo(int x_inicial, int x_final, int y_inicial, int y_final) {
        this.setBackground(color.DARK_GRAY);
    	this.objeto = this;
    	this.ar = new Arrastrar(this);
    	this.addMouseMotionListener(ar);
    	this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 1) {
					slider.setObjeto(objeto);
					slider.setValue(escala);
					rotar_izquierda.setObjeto(objeto);
					rotar_derecha.setObjeto(objeto);
				}
			}
		});
    	
    	this.ancho_inicial = Math.abs(x_final - x_inicial);
		this.alto_inicial = Math.abs(y_final - y_inicial);
    	
    	this.puntos = new Puntos();
    	this.x_inicial = x_inicial;
    	this.x_final = x_final;
    	this.y_inicial = y_inicial;
    	this.y_final = y_final;
    	this.Calcular();
    }

	@Override
	public void Mover(int x, int y) {
		// TODO Auto-generated method stub
		if ((y > 32) && ((y+alto) < 568) && (x > 0) && ((x+ancho) < 800)) {
			this.x_inicial = x;
			this.y_inicial = y;
			this.setLocation(x, y);
		}
	}

	@Override
	public void Tamano(int width, int height) {
		// TODO Auto-generated method stub
		this.setSize(width, height);
		this.ancho = width;
		this.alto = height;
		this.x_final = this.x_inicial + this.ancho;
		this.y_final = this.y_inicial + this.alto;
	}

	@Override
	public void Calcular() {
		// TODO Auto-generated method stub
		int tmp;
		float pm;
    	float x = 0; int y = 0;
    	double diferencia_x, diferencia_y;
    	boolean x_termino = false, y_termino = false, terminado = false;
    	double incremento_x = 1, incremento_y = 1;

		if (this.x_inicial > this.x_final)
		{
			tmp = this.x_final;
			this.x_final = this.x_inicial;
			this.x_inicial = tmp;
		}
		
		if (this.y_inicial > this.y_final)
		{
			tmp = this.y_final;
			this.y_final = this.y_inicial;
			this.y_inicial = tmp;
		}
		
		this.ancho = this.x_final - this.x_inicial;
		this.alto = this.y_final - this.y_inicial;
		this.Tamano(this.ancho, this.alto);
		this.setLocation(this.x_inicial, this.y_inicial + 32);
		
		int xi, yi, xf, yf;
		xi = 0;
		yi = 0;
		xf = this.ancho;
		yf = this.alto;
		
		//Punto Medio
		pm = ancho /2;
        
        // Inicio de trazo del punto medio al cuadrante derecho
        x = pm;
        y = yi;
        
        diferencia_x = (pm - xi);
        diferencia_y = (yf - yi);
        
        if (diferencia_x < 0)
        	diferencia_x = diferencia_x * (-1);
        
        if (diferencia_x > diferencia_y)
        	incremento_y = diferencia_y / diferencia_x;
        else
        	incremento_x = diferencia_x / diferencia_y;
        
        do {
    		
    		if (!x_termino) {
    			if (x <= xf) {
    				x+=incremento_x;
    				if (x > xf)
    					x_termino = true;
    			} else {
    				x-=incremento_x;
    				if (x < xf)
    					x_termino = true;
    			}
    			
    		}
    		if (!y_termino) {
    			if (y <= yf) {
    				y+=incremento_y;
    				if (y > yf)
    					y_termino = true;
    			} else {
    				y-=incremento_y;
    				if (y < yf)
    					y_termino = true;
    			}
    		} 

    		puntos.addPunto(x, y);
    		
    		if (x_termino == true && y_termino == true) {
    			terminado = true;
    		}
    		
    	} while (terminado == false);
        
        // Inicio de trazo del punto medio al cuadrante derecho
        
        x = pm;
        y = yi;
                
        // Reseteo de banderas
        x_termino = false;
        y_termino = false;
        terminado = false;
        
        diferencia_x = (pm - xi);
        
        if (diferencia_x < 0)
        	diferencia_x = diferencia_x * (-1);
        
        if (diferencia_x > diferencia_y)
        	incremento_y = diferencia_y / diferencia_x;
        else
        	incremento_x = diferencia_x / diferencia_y;
        
        do {
    		
    		if (!x_termino) {
    			if (x <= xi) {
    				x+=incremento_x;
    				if (x > xi)
    					x_termino = true;
    			} else {
    				x-=incremento_x;
    				if (x < xi)
    					x_termino = true;
    			}
    			
    		}
    		if (!y_termino) {
    			if (y <= yf) {
    				y+=incremento_y;
    				if (y > yf)
    					y_termino = true;
    			} else {
    				y-=incremento_y;
    				if (y < yf)
    					y_termino = true;
    			}
    		} 

    		puntos.addPunto(x, y);
    		
    		if (x_termino == true && y_termino == true) {
    			terminado = true;
    		}
    		
    	} while (terminado == false);
        
        for (x = 0; x <= this.ancho; x++) {
        	puntos.addPunto(x, this.alto-1);
        }
	}
	
	public void paint(Graphics g)
    {
            dibujaTrazo(g);
    }
	
	public void update(Graphics g)
    {
        paint(g);
    }
	
	
	private void dibujaTrazo(Graphics g)
    {	
		double p0x, p0y, p1x, p1y;
		double pmx, pmy;
		double vp1x, vp1y, vp2x, vp2y;
		double vector1, vector2;
		double av1, av2;
		double minx = 0, maxx = this.ancho, miny = 0, maxy = this.alto;
		
		pmx = this.ancho/2;
		pmy = this.alto/2;
		
        g.setColor(this.color);
    	Point2D p0 = this.puntos.getPunto(0);
    	
        for (int i = 0; i < this.puntos.getSize() - 1; i++)
        {
            Point2D p1 = this.puntos.getPunto(i);
            
            p0x = (int) p0.getX();
            p0y = (int) p0.getY();
            p1x = (int) p1.getX();
            p1y = (int) p1.getY();
            
            //vp1x = pmx - p0x;
            //vp1y = pmy - p0y;
            
            vp1x = p0x;
            vp1y = p0y;
            
            vector1 = Math.sqrt((vp1x * vp1x) + (vp1y * vp1y));
            
            // Angulo formado entre el punto medio y P0
            av1 = Math.atan(vp1y/vp1x);
            av1 = av1 + this.angulo;
            
            p0x = (vector1 * Math.cos(av1));
            p0y = (vector1 * Math.sin(av1));
            
            // Calculo del vector de posición V1x y V1y
            
            //vp2x = p1x - pmx;
            //vp2y = p1y - pmy;
            
            vp2x = p1x;
            vp2y = p1y;
            
            vector2 = Math.sqrt((vp2x * vp2x) + (vp2y * vp2y));
            
            // Angulo formado entre el punto medio y P0
            
            av2 = Math.atan(vp2y/vp2x);
            av2 = av2 + this.angulo;
            
            p1x = (vector2 * Math.cos(av2));
            p1y = (vector2 * Math.sin(av2));
            
            if (p0x < minx) {
            	minx = p0x;
            }
            
            if (p1x > maxx) {
            	maxx = p1x;
            }
            
            if (p0y < miny) {
            	miny = p0y;
            }
            
            if (p1y > maxy) {
            	maxy = p1y;
            }
            
            g.drawLine((int) p0x, (int) p0y, (int) p1x,(int) p1y);
            p0 = p1;
        }
        //this.setSize((int)this.ancho*2, (int)this.alto*2);
        
    }
	
	/*private void dibujaTrazo(Graphics g)
    {	
        g.setColor(this.color);
        
    	Point2D p0 = this.puntos.getPunto(0);
        for (int i = 0; i < this.puntos.getSize() - 1; i++)
        {
            Point2D p1 = this.puntos.getPunto(i);
            g.drawLine((int) p0.getX(), (int) p0.getY(), (int) p1.getX(),
                    (int) p1.getY());
            p0 = p1;
        }
    }*/

	@Override
	public void Recalcular() {
		// TODO Auto-generated method stub
		Graphics g = this.getGraphics();
		g.clearRect(0, 0, ancho, alto);
		puntos = null;
		this.puntos = new Puntos();
		Calcular();
		this.paint(g);
	}
	
	public Graphics getGraficos() {
		return this.getGraphics();
	}

	@Override
	public void dibujar() {
		// TODO Auto-generated method stub
		this.repaint();
	}

	@Override
	public void setColor(Color color) {
		// TODO Auto-generated method stub
		this.color = color;
	}

	public int getX()
	{
		return this.x_inicial;
	}
	
	public int getY()
	{
		return this.y_inicial;
	}
	
	public int getWidth()
	{
		//System.out.println(this.width);
		return this.ancho;
	}
	
	public int getHeight()
	{
		return this.alto;
	}

	@Override
	public void setSlider(Slider slider) {
		// TODO Auto-generated method stub
		this.slider = slider;
		this.slider.setValue(this.escala);
	}

	@Override
	public void setEscala(int escala) {
		// TODO Auto-generated method stub
		this.escala = escala;
	}

	@Override
	public int getAngulo() {
		// TODO Auto-generated method stub
		return this.angulo;
	}

	@Override
	public void setAngulo(int angulo) {
		// TODO Auto-generated method stub
		this.angulo = angulo;
	}

	@Override
	public void setRotador(BotonRotar rotador, String accion) {
		// TODO Auto-generated method stub
		if (accion == "+")
			this.rotar_izquierda = rotador;
		else
			this.rotar_derecha = rotador;
	}

	@Override
	public ContenedorFigura getPanel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Actualizar(Graphics g) {
		// TODO Auto-generated method stub
		this.paint(g);
	}

	@Override
	public int getAnchoInicial() {
		// TODO Auto-generated method stub
		return this.ancho_inicial;
	}

	@Override
	public int getAltoInicial() {
		// TODO Auto-generated method stub
		return this.alto_inicial;
	}

	@Override
	public void setPanel(ContenedorFigura panel) {
		// TODO Auto-generated method stub
		
	}
}
