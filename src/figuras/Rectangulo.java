package figuras;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import objetos.BotonRotar;
import objetos.ContenedorFigura;
import objetos.Slider;
import eventos.EventosObjetos;
import eventos.Arrastrar;

public class Rectangulo extends Canvas implements EventosObjetos {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Arrastrar ar;
	private int x_inicial = -1;
    private int x_final = -1;
    private int y_inicial = -1;
    private int y_final = -1;
    private int ancho;
    private int alto;
    private int ancho_inicial;
    private int alto_inicial;
    
    public EventosObjetos objeto;
    private Slider slider;
    private int escala = 100;
    private int angulo = 0;
    private BotonRotar rotar_izquierda;
    private BotonRotar rotar_derecha;
    
    private Puntos puntos;
    private Color color = Color.RED;
    public ContenedorFigura panel;
    BufferedImage img;

    public Rectangulo(int x_inicial, int x_final, int y_inicial, int y_final) {
        this.setBackground(color.DARK_GRAY);
    	this.objeto = this;
    	this.ar = new Arrastrar(this);
    	this.addMouseMotionListener(ar);
    	this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 1) {
					slider.setObjeto(objeto);
					slider.setValue(escala);
					rotar_izquierda.setObjeto(objeto);
					rotar_derecha.setObjeto(objeto);
				}
			}
		});
    	
    	this.ancho_inicial = Math.abs(x_final - x_inicial);
		this.alto_inicial = Math.abs(y_final - y_inicial);
		img = new BufferedImage(ancho_inicial, alto_inicial, BufferedImage.TYPE_INT_RGB);
    	
    	this.puntos = new Puntos();
    	this.x_inicial = x_inicial;
    	this.x_final = x_final;
    	this.y_inicial = y_inicial;
    	this.y_final = y_final;
    	this.Calcular();
    }
    
	public void Mover(int x, int y) {
		// TODO Auto-generated method stub
		if ((y > 32) && ((y+alto) < 568) && (x > 0) && ((x+ancho) < 800)) {
			this.x_inicial = x;
			this.y_inicial = y;
			this.setLocation(x, y);
			//this.panel.Mover(x, y);
		}
	}

	public void Tamano(int width, int height) {
		// TODO Auto-generated method stub
		this.setSize(width, height);
		this.ancho = width;
		this.alto = height;
		this.x_final = this.x_inicial + this.ancho;
		this.y_final = this.y_inicial + this.alto;
	}

	public void Calcular() {
		// TODO Auto-generated method stub
		int tmp;
		int x, y;

		if (this.x_inicial > this.x_final)
		{
			tmp = this.x_final;
			this.x_final = this.x_inicial;
			this.x_inicial = tmp;
		}
		
		if (this.y_inicial > this.y_final)
		{
			tmp = this.y_final;
			this.y_final = this.y_inicial;
			this.y_inicial = tmp;
		}
		
		this.ancho = this.x_final - this.x_inicial;
		this.alto = this.y_final - this.y_inicial;
		
		this.Tamano(this.ancho, this.alto);
		this.setLocation(this.x_inicial, this.y_inicial + 32);
        
		for (x = 0; x <= this.ancho; x++) {
        	this.puntos.addPunto(x, 0);
        	this.puntos.addPunto(x, this.alto);
        }
        
        for (y = 0; y <= this.alto; y++) {
        	this.puntos.addPunto(0, y);
        	this.puntos.addPunto(this.ancho, y);
        }
	}
	
	@Override
	public void paint(Graphics g)
	{
		dibujaTrazo(g);
	}
	
	public void update(Graphics g)
    {
        paint(g);
    }
	
	private void dibujaTrazo(Graphics g)
    {			
		Graphics2D g2 = (Graphics2D)g;
        g2.setColor(this.color);
        double p0x, p0y, p1x, p1y;
        double vector1, vector2;
        double vp1x, vp1y, vp2x, vp2y;
        double av1, av2;
        double pmx, pmy;
        double minx = 0, maxx = this.ancho, miny = 0, maxy = this.alto;
        double sumax, sumay;
        
        pmx = this.ancho / 2;
        pmy = this.alto / 2;
        
        Point2D p0 = this.puntos.getPunto(0);
        
        for (int i = 0; i < this.puntos.getSize() - 1; i++)
        {
        	Point2D p1 = this.puntos.getPunto(i);
        	
        	p0x = (int) p0.getX();
            p0y = (int) p0.getY();
            p1x = (int) p1.getX();
            p1y = (int) p1.getY();
            
            // Calculo del vector de posición V1x y V1y
            
            vp1x = p0x - pmx;
            vp1y = p0y - pmy;
            
            vector1 = Math.sqrt((vp1x * vp1x) + (vp1y * vp1y));
            
            // Angulo formado entre el punto medio y P0
            av1 = Math.atan(vp1y/vp1x);
            av1 = av1 + this.angulo;
            
            p0x = pmx + (vector1 * Math.cos(av1));
            p0y = pmy + (vector1 * Math.sin(av1));
            
            // Calculo del vector de posición V1x y V1y
            
            vp2x = p1x - pmx;
            vp2y = p1y - pmy;
            
            vector2 = Math.sqrt((vp2x * vp2x) + (vp2y * vp2y));
            
            // Angulo formado entre el punto medio y P0
            
            av2 = Math.atan(vp2y/vp2x);
            av2 = av2 + this.angulo;
            
            p1x = pmx + (vector2 * Math.cos(av2));
            p1y = pmy + (vector2 * Math.sin(av2));
            
            if (p0x < minx) {
            	minx = p0x;
            }
            
            if (p1x > maxx) {
            	maxx = p1x;
            }
            
            if (p0y < miny) {
            	miny = p0y;
            }
            
            if (p1y > maxy) {
            	maxy = p1y;
            }

            g2.drawLine((int) p0x, (int) p0y, (int) p1x, (int) p1y);
        	
            p0 = p1;
        }
        
        sumax = maxx + Math.abs(minx) + this.ancho;
        sumay = maxy + Math.abs(miny) + this.alto;
        
        System.out.println("->" + sumax + " " + sumay + " " + maxx + " " + maxy + " " + minx + " " + miny + " " + this.ancho + " " + this.alto);
        //this.setSize((int)sumax, (int)sumay);
        //this.Tamano((int)sumax, (int)sumay);
        
    }
	
	public void setColor(Color color)
	{
		this.color = color;
	}
	
	public int getX()
	{
		return this.x_inicial;
	}
	
	public int getY()
	{
		return this.y_inicial;
	}
	
	public int getWidth()
	{
		//System.out.println(this.width);
		return this.ancho;
	}
	
	public int getHeight()
	{
		return this.alto;
	}
	
	@Override
	public void dibujar()
	{
		this.repaint();
	}
	
	@Override
	public void Recalcular() {
		// TODO Auto-generated method stub
		Graphics g = this.getGraphics();
		g.clearRect(0, 0, ancho, alto);
		puntos = null;
		this.puntos = new Puntos();
		Calcular();
		this.paint(g);
	}
	
	public Graphics getGraficos() {
		return this.getGraphics();
	}

	@Override
	public void setSlider(Slider slider) {
		// TODO Auto-generated method stub
		this.slider = slider;
		this.slider.setValue(this.escala);
	}

	@Override
	public void setEscala(int escala) {
		// TODO Auto-generated method stub
		this.escala = escala;
	}

	@Override
	public int getAngulo() {
		// TODO Auto-generated method stub
		//System.out.println("getAngulo " + angulo);
		return this.angulo;
	}

	@Override
	public void setAngulo(int angulo) {
		// TODO Auto-generated method stub
		this.angulo = angulo;
		//System.out.println("setAngulo " + angulo);
	}

	@Override
	public void setRotador(BotonRotar rotador, String accion) {
		// TODO Auto-generated method stub
		if (accion == "+")
			this.rotar_izquierda = rotador;
		else
			this.rotar_derecha = rotador;
	}
	
	@Override
	public void setPanel(ContenedorFigura panel)
	{
		this.panel = panel;
		//this.panel.setBounds(x_inicial, y_inicial, ancho, alto);
		//this.setLocation(this.x_inicial, this.y_inicial + 32);
	}
	
	public ContenedorFigura getPanel()
	{
		return this.panel;
	}

	@Override
	public void Actualizar(Graphics g) {
		// TODO Auto-generated method stub
		this.paint(g);
	}

	@Override
	public int getAnchoInicial() {
		// TODO Auto-generated method stub
		return this.ancho_inicial;
	}

	@Override
	public int getAltoInicial() {
		// TODO Auto-generated method stub
		return this.alto_inicial;
	}
}