package eventos;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import objetos.Panel;

public class ListenerArrastre implements MouseMotionListener
{
    private Panel panel;
    
    boolean arrastrando = false;
    boolean primerMovimiento = false;
    int xInicial;
    int xFinal;
    int yInicial;
    int yFinal;
    
    public ListenerArrastre(Panel panel)
    {
        //this.accion = accion;
    	this.panel = panel;
    }

    public void mouseMoved(MouseEvent e)
    {
    	xFinal = e.getX();
        yFinal = e.getY();
    	if (this.arrastrando == true) {
    		panel.setCoords(xInicial, yInicial, xFinal, yFinal);
    		panel.Dibujar();
    	}
        arrastrando = false;
    }

    public void mouseDragged(MouseEvent e)
    {
    	//if (this.panel.enaClick == true) {
		if (this.arrastrando == false) {
    		this.xInicial = e.getX();
    		this.yInicial = e.getY();
        	this.arrastrando = true;
        }
            //accion.arrastra(xAntigua, yAntigua, e.getX(), e.getY());
        xFinal = e.getX();
        yFinal = e.getY();
    	//}
    }
}