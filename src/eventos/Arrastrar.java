package eventos;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import eventos.EventosObjetos;

public class Arrastrar implements MouseMotionListener 
{

	private EventosObjetos objeto;
	private int posx;
	private int posy;
	
	int xInicial;
    int xFinal;
    int yInicial;
    int yFinal;
    int width;
    int heigth;
    
    int xos;
    int yos;
    int dxos;
    int dyos;
	
	public Arrastrar(EventosObjetos objeto) 
	{
		this.objeto = objeto;
		//JPanel panel = objeto.getPanel();
		
		//this.xInicial = panel.getX();
		//this.yInicial = panel.getY();
		//this.width = panel.getWidth();
		//this.heigth = panel.getHeight();
		
		this.xInicial = objeto.getX();
		this.yInicial = objeto.getY();
		this.width = objeto.getWidth();
		this.heigth = objeto.getHeight();
	}
	
	public int getX()
	{
		return this.posx;
	}
	
	public int getY()
	{
		return this.posy;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
    /*    if (arrastre == false)
        {
            arrastre = true;
            xFinal = 0;
            yFinal = 0;
            this.xInicial = objeto.getX();
    		this.yInicial = objeto.getY();
        }
        xFinal = e.getX();
        yFinal = e.getY();*/
		objeto.Mover(objeto.getX() + e.getX() - objeto.getWidth() / 2,objeto.getY() + e.getY() - objeto.getHeight() / 2);
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
	/*	e.getLocationOnScreen();
		// TODO Auto-generated method stub
        if (arrastre == true) {
        	
        	xos = e.getPoint().x;
    		yos = e.getPoint().y;
        	
    		dxos = xFinal - xos;
    		dyos = yFinal - yos;
    		System.out.println(xos + " - " + yos);
        	objeto.Mover(xFinal, yFinal);
        }
        arrastre = false;
        xFinal = e.getX();
        yFinal = e.getY();
      */  
	}
	
}
